import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
    home: new HalamanSatu(),
    title: "Another Tutorial",
    routes: <String, WidgetBuilder>{
      '/HalamanSatu': (BuildContext context) => new HalamanSatu(),
      '/HalamanDua': (BuildContext context) => new HalamanDua(),
    },
  ));
}

class HalamanSatu extends StatelessWidget {
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Card & Parsing"),
        ),
        body: new Container(
          child: new Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                new CardSaya(
                  icon: Icons.home,
                  teks: "Home",
                  warnaIcon: Colors.brown,
                ),
                new CardKamu(
                  icon: Icons.favorite_border,
                  teks: "Favorite ",
                  warnaIcon: Colors.pink,
                ),
                new CardSaya(
                  icon: Icons.place,
                  teks: "Place",
                  warnaIcon: Colors.blue,
                ),
                new CardSaya(
                  icon: Icons.home,
                  teks: "Setting",
                  warnaIcon: Colors.black,
                ),
              ]),
        ));
  }
}

class CardSaya extends StatelessWidget {
  CardSaya({required this.icon, required this.teks, required this.warnaIcon});
  final IconData icon;
  final String teks;
  final Color warnaIcon;

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: new EdgeInsets.all(10.0),
      child: new Card(
        child: new IconButton(
          icon: new Icon(
            Icons.local_pizza_rounded,
            size: 40.0,
            color: Colors.brown,
          ),
          onPressed: () {
            Navigator.pushNamed(context, '/HalamanDua');
          },
        ),
      ),
    );
  }
}

class CardKamu extends StatelessWidget {
  CardKamu({required this.icon, required this.teks, required this.warnaIcon});
  final IconData icon;
  final String teks;
  final Color warnaIcon;

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: const EdgeInsets.all(10.0),
      child: new Card(
        child: new IconButton(
          icon: new Icon(
            Icons.android,
            size: 40.0,
            color: Colors.orangeAccent,
          ),
          onPressed: () {
            Navigator.pushNamed(context, '/HalamanDua');
          },
        ),
      ),
    );
  }
}

class HalamanDua extends StatelessWidget {
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Test 1"),
      ),
      body: new Container(
        child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              new IconButton(
                icon: new Icon(
                  Icons.speaker_rounded,
                  size: 150.0,
                  color: Colors.brown,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/HalamanSatu');
                },
              ),
            ]),
      ),
    );
  }
}
